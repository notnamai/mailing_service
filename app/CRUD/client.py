from typing import (
    List,
    Optional,
    Union,
    )

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import Client
from app.schema.client import (
    ClientBaseSchema,
    ClientCreateSchema,
    )


async def create_client(db: AsyncSession, new_client: ClientCreateSchema) -> Client:

    client = Client(**new_client.dict())
    db.add(client)
    await db.commit()
    return client


async def read_clients(db: AsyncSession) -> Union[None, Client, List[Client]]:
    return (await db.execute(select(Client))).scalars()


async def update_client(db: AsyncSession, new_data: ClientBaseSchema, client_id: int) -> Optional[Client]:

    client = (
        await db.execute(select(Client).where(Client.id == client_id))
    ).scalar_one_or_none()
    if client is None:
        return None
    client.phone_number = new_data.phone_number
    client.mobile_operator_code = new_data.mobile_operator_code
    client.tag = new_data.tag
    client.time_zone = new_data.time_zone
    await db.commit()
    return client


async def delete_clients(db: AsyncSession, client_id: int) -> Optional[bool]:

    delete_client = (
        await db.execute(select(Client).where(Client.id == client_id))
    ).scalar_one_or_none()
    if delete_client is None:
        return None
    await db.delete(delete_client)
    await db.commit()
    return True

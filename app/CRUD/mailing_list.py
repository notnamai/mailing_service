from typing import (
    List,
    Optional,
    Union,
    )

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import MailingList
from app.schema.mailing_list import (
    MailingListBaseSchema,
    MailingListCreateSchema,
    )


async def create_mailing_list(db: AsyncSession, new_mailing_list: MailingListCreateSchema) -> MailingList:

    mailing_list = MailingList(
                    date_created=new_mailing_list.date_created,
                    text_message=new_mailing_list.text_message,
                    filter=new_mailing_list.filter,
                    end_date=new_mailing_list.end_date,
                    )
    db.add(mailing_list)
    await db.commit()
    return mailing_list


async def read_mailing_list(db: AsyncSession, mailing_list_id: Optional[int] = None) -> Union[None, MailingList, List[MailingList]]:

    if mailing_list_id:
        return (await db.execute(select(MailingList).where(MailingList.id == mailing_list_id))).scalar_one_or_none()
    return (await db.execute(select(MailingList))).scalars()


async def update_mailing_list(db: AsyncSession, new_data: MailingListBaseSchema, mailing_list_id: int) -> Optional[MailingList]:

    mailing_list = (
        await db.execute(select(MailingList).where(MailingList.id == mailing_list_id))
    ).scalar_one_or_none()
    if mailing_list is None:
        return None
    mailing_list.date_created = new_data.date_created
    mailing_list.text_message = new_data.text_message
    mailing_list.filter = new_data.filter
    mailing_list.end_date = new_data.end_date
    await db.commit()
    return mailing_list


async def delete_mailing_lst(db: AsyncSession, mailing_list_id: int) -> Optional[bool]:

    delete_mailing_list = (
        await db.execute(select(MailingList).where(MailingList.id == mailing_list_id))
    ).scalar_one_or_none()
    if delete_mailing_list is None:
        return None
    await db.delete(delete_mailing_list)
    await db.commit()
    return True

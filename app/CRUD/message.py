from typing import (
    List,
    Optional,
    Union,
    )

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import (
    Client,
    MailingList,
    Message,
    )
from app.schema.message import (
    MessageBaseSchema,
    MessageCreateSchema,
    )


async def create_message(db: AsyncSession, new_message: MessageCreateSchema) -> tuple[bool, Union[str, Message]]:

    mailing_list = (
        await db.execute(select(MailingList).where(MailingList.id == new_message.mailing_list_id))
    ).scalar_one_or_none()
    if mailing_list is None:
        return False, 'This mailing list {mailing_list} not exist'.format(mailing_list=new_message.mailing_list_id)
    client_id = (await db.execute(select(Client).where(Client.id == new_message.client_id))).scalar_one_or_none()
    if client_id is None:
        return False, 'This client {client} not exist'.format(client=new_message.client_id)
    message = Message(**new_message.dict())
    db.add(message)
    await db.commit()
    return True, message


async def read_messages(db: AsyncSession, mailing_list_id: Optional[int] = None) -> Union[None, Message, List[Message]]:
    query = select(Message)
    if mailing_list_id is not None:
        query = query.where(Message.mailing_list_id == mailing_list_id)
    return (await db.execute(query)).scalars()


async def update_message(db: AsyncSession, new_data: MessageBaseSchema, message_id: int) -> Optional[Message]:

    message = (
        await db.execute(select(Message).where(Message.id == message_id))
    ).scalar_one_or_none()
    if message is None:
        return None
    message.date_create_send = new_data.date_create_send
    message.status = new_data.status
    message.mailing_list_id = new_data.mailing_list_id
    message.client_id = new_data.client_id
    await db.commit()
    return message


async def delete_messages(db: AsyncSession, message_id: int) -> Optional[bool]:

    delete_message = (
        await db.execute(select(Message).where(Message.id == message_id))
    ).scalar_one_or_none()
    if delete_message is None:
        return None
    await db.delete(delete_message)
    await db.commit()
    return True

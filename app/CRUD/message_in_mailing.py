from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import (
    MailingList,
    Message,
    )


async def read_message_in_mailing(db: AsyncSession):
    return (await db.execute(select(MailingList, Message.id.count.label('count')))
            ).join(Message, MailingList.id == Message.mailing_list_id).group_by(MailingList.id, Message.status).all()

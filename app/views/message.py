from typing import Optional

from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    )
from sqlalchemy.ext.asyncio import AsyncSession

from app.CRUD.message import (
    create_message,
    delete_messages,
    read_messages,
    update_message,
    )
from app.models.database import get_db
from app.schema.message import (
    MessageBaseSchema,
    MessageCreateSchema,
    MessageSchema,
    )

router = APIRouter(prefix='/message', tags=['Message'])

dep_get_db = Depends(get_db)
not_found_error = 404


@router.post('/', response_model=MessageSchema)
async def message_post(new_message: MessageCreateSchema, db: AsyncSession = dep_get_db):

    success, message = await create_message(db, new_message)
    if not success:
        raise HTTPException(status_code=not_found_error, detail=message)
    return MessageSchema.from_orm(message)


@router.get('/', response_model=list[MessageSchema])
async def messages_get(db: AsyncSession = dep_get_db, mailing_list_id: Optional[int] = None):

    message = await read_messages(db, mailing_list_id)
    return [MessageSchema.from_orm(i_message) for i_message in message]


@router.put('/{message_id}', response_model=MessageSchema)
async def messages_put_detail(
    message_id: int,
    message: MessageBaseSchema,
    db: AsyncSession = dep_get_db,
):

    message = await update_message(db, message, message_id)
    if message is None:
        raise HTTPException(status_code=not_found_error, detail='Message not found')
    return MessageSchema.from_orm(message)


@router.delete('/{message_id}', response_model=str)
async def messages_delete(
    message_id: int,
    db: AsyncSession = dep_get_db,
):

    is_message_delete = await delete_messages(db, message_id)
    if not is_message_delete:
        raise HTTPException(status_code=not_found_error, detail='Message not found')
    return 'Message delete'

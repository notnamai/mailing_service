from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    )
from sqlalchemy.ext.asyncio import AsyncSession

from app.CRUD.client import (
    create_client,
    delete_clients,
    read_clients,
    update_client,
    )
from app.models.database import get_db
from app.schema.client import (
    ClientBaseSchema,
    ClientCreateSchema,
    ClientSchema,
    )

router = APIRouter(prefix='/client', tags=['Client'])

dep_get_db = Depends(get_db)
not_found_error = 404


@router.post('/', response_model=ClientSchema)
async def client_post(new_client: ClientCreateSchema, db: AsyncSession = dep_get_db):

    client = await create_client(db, new_client)
    return ClientSchema.from_orm(client)


@router.get('/', response_model=list[ClientSchema])
async def clients_get(db: AsyncSession = dep_get_db):

    client = await read_clients(db)
    return [ClientSchema.from_orm(i_client) for i_client in client]


@router.put('/{client_id}', response_model=ClientSchema)
async def products_put_detail(
    client_id: int,
    client: ClientBaseSchema,
    db: AsyncSession = dep_get_db,
):

    client = await update_client(db, client, client_id)
    if client is None:
        raise HTTPException(status_code=not_found_error, detail='Client not found')
    return ClientSchema.from_orm(client)


@router.delete('/{product_id}', response_model=str)
async def clients_delete(
    client_id: int,
    db: AsyncSession = dep_get_db,
):

    is_client_delete = await delete_clients(db, client_id)
    if not is_client_delete:
        raise HTTPException(status_code=not_found_error, detail='Client not found')
    return 'Client delete'

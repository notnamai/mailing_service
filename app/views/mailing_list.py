from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    )
from sqlalchemy.ext.asyncio import AsyncSession

from app.CRUD.mailing_list import (
    create_mailing_list,
    delete_mailing_lst,
    read_mailing_list,
    update_mailing_list,
    )
from app.models.database import get_db
from app.schema.mailing_list import (
    MailingListBaseSchema,
    MailingListCreateSchema,
    MailingListSchema,
    )

router = APIRouter(prefix='/mailing_list', tags=['MailingList'])

dep_get_db = Depends(get_db)
not_found_error = 404


@router.post('/', response_model=MailingListSchema)
async def mailing_list_post(new_mailing_list: MailingListCreateSchema, db: AsyncSession = dep_get_db):

    mailing_list = await create_mailing_list(db, new_mailing_list)
    return MailingListSchema.from_orm(mailing_list)


@router.get('/', response_model=list[MailingListSchema])
async def mailing_list_get(db: AsyncSession = dep_get_db):

    mailing_list = await read_mailing_list(db)
    return [MailingListSchema.from_orm(i_mailing_list) for i_mailing_list in mailing_list]


@router.get('/{mailing_list_id}', response_model=MailingListSchema)
async def mailing_list_get_detail(
    mailing_list_id: int,
    db: AsyncSession = dep_get_db,
):
    mailing_list = await read_mailing_list(db, mailing_list_id=mailing_list_id)
    if mailing_list is None:
        raise HTTPException(status_code=not_found_error, detail='Mailing list not found')
    return MailingListSchema.from_orm(mailing_list)


@router.put('/{mailing_list_id}', response_model=MailingListSchema)
async def mailing_list_put_detail(
    mailing_list_id: int,
    mailing_list: MailingListBaseSchema,
    db: AsyncSession = dep_get_db,
):

    mailing_list = await update_mailing_list(db, mailing_list, mailing_list_id)
    if mailing_list is None:
        raise HTTPException(status_code=not_found_error, detail='Client not found')
    return MailingListSchema.from_orm(mailing_list)


@router.delete('/{mailing_list_id}', response_model=str)
async def mailing_list_delete(
    mailing_list_id: int,
    db: AsyncSession = dep_get_db,
):

    is_mailing_list_delete = await delete_mailing_lst(db, mailing_list_id)
    if not is_mailing_list_delete:
        raise HTTPException(status_code=not_found_error, detail='Mailing list not found')
    return 'Mailing list delete'

from typing import List

from fastapi import (
    APIRouter,
    Depends,
    )
from sqlalchemy import alias
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import (
    func,
    select,
    )

from app.CRUD.message_in_mailing import read_message_in_mailing
from app.models import (
    MailingList,
    Message,
    )
from app.models.database import get_db
from app.schema.message_in_mailing import MessageInMailingSchema

router = APIRouter(prefix='/message_mailing', tags=['RPC/'])
dep_get_db = Depends(get_db)


@router.get('/')
async def message_mailing_get(db: AsyncSession = dep_get_db) -> List[MessageInMailingSchema]:

    result = (
        await db.execute(
            select(MailingList.text_message.label('text'), MailingList.filter.label('filter'),
                   MailingList.id.label('id'), MailingList.end_date.label('end_date'),
                   MailingList.date_created.label('date_created'),Message.status, func.count(Message.id)
                   ).join(Message, MailingList.id == Message.mailing_list_id).group_by(MailingList.id, Message.status))
    ).all()
    return [MessageInMailingSchema.from_orm(i) for i in result]

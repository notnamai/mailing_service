from datetime import datetime
from typing import (
    Any,
    Optional,
    Union,
    )

from pydantic import BaseModel


class MessageInMailingSchema(BaseModel):
    id: Optional[int] = None
    text: Union[str, tuple, Any] = None
    filter: Optional[str] = None
    status: Optional[str] = None
    count: Optional[int] = None
    end_date: Optional[datetime] = None
    date_created: Optional[datetime] = None

    class Config:
        orm_mode = True



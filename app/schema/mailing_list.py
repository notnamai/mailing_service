from datetime import datetime

from pydantic import BaseModel


class MailingListBaseSchema(BaseModel):
    date_created: datetime
    text_message: str
    filter: str
    end_date: datetime


class MailingListCreateSchema(MailingListBaseSchema):
    pass


class MailingListSchema(MailingListBaseSchema):
    id: int

    class Config:
        orm_mode = True

from typing import Optional

from pydantic import BaseModel


class ClientBaseSchema(BaseModel):

    phone_number: Optional[str] # добавить проверку телефонного номера
    mobile_operator_code: str
    tag: str
    time_zone: Optional[float] = None


class ClientCreateSchema(ClientBaseSchema):
    pass


class ClientSchema(ClientBaseSchema):

    id: int

    class Config:
        orm_mode = True


from datetime import datetime

from pydantic import BaseModel


class MessageBaseSchema(BaseModel):
    date_create_send: datetime
    status: str
    mailing_list_id: int
    client_id: int


class MessageCreateSchema(MessageBaseSchema):
    pass


class MessageSchema(MessageBaseSchema):
    id: int

    class Config:
        orm_mode = True

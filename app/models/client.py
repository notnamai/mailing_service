from sqlalchemy import (
    BigInteger,
    Column,
    Float,
    Integer,
    String,
    )
from sqlalchemy.util import timezone
from sqlalchemy_utils import PhoneNumberType

from app.models.database import Base


class Client(Base):

    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True)
    phone_number = Column(String)
    mobile_operator_code = Column(String)
    tag = Column(String)
    time_zone = Column(Float)

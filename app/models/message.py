from typing import TYPE_CHECKING

from sqlalchemy import (
    TIMESTAMP,
    Column,
    ForeignKey,
    Integer,
    String,
    )
from sqlalchemy.orm import relationship

from app.models.database import Base

if TYPE_CHECKING:
    from app.models.mailing_list import MailingList


class Message(Base):

    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    date_create_send = Column(type_=TIMESTAMP(timezone=True))
    status = Column(String)
    mailing_list_id = Column(Integer, ForeignKey('mailing_lists.id', ondelete='CASCADE'), nullable=False)
    client_id = Column(Integer, ForeignKey('clients.id', ondelete='CASCADE'), nullable=False)
    mailing_list: 'MailingList' = relationship('MailingList', backref='messages')
    client = relationship('Client', backref='messages')

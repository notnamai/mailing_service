
from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    String,
    )
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.types import TIMESTAMP

from app.models.database import Base


class MailingList(Base):

    __tablename__ = 'mailing_lists'

    id = Column(Integer, primary_key=True)
    date_created = Column(type_=TIMESTAMP(timezone=True))
    text_message = Column(String)
    filter = Column(JSONB)
    end_date = Column(type_=TIMESTAMP(timezone=True))

"""Файл содержит главные узлы микросервиса по управлению рассылками."""
import uvicorn
from fastapi import FastAPI

from app.models.database import recreate_db
from app.views import (
    client,
    mailing_list,
    message,
    message_in_mailing,
    )

app = FastAPI()
port = 5001


@app.on_event('startup')
async def startup():
    """При старте приложения функция создает соединение к БД."""
    recreate_db()

app.include_router(client.router)
app.include_router(mailing_list.router)
app.include_router(message.router)
app.include_router(message_in_mailing.router)


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=port)

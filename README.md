
## Сервис смс рассылок  

#### Данное приложение разделено на два микросервиса, в одном крутиться база данных и все что связяно с созданием смс рассылки (сущности, CRUD и т.д.), в другом demon слушает создание новых смс рассылок и если они созданы, отправляет их по внешнему API.  
#### Программа разработана на фреймворке FastAPI
#### В качестве базы данных используется PostgreSQL

## запуск приложения на локальной машине

В корне проекта:

1. Ставим pyenv для активации python 3.9.13 https://linux-notes.org/ustanovka-pyenv-v-unix-linux/

   1.1. Обновляем pyenv `cd /home/znbiz/.pyenv/plugins/python-build/../.. && git pull && cd -`

   1.2. Ставим нужную версию python `pyenv install 3.9.13` (инструкция для mac в случае проблемы установки https://github.com/pyenv/pyenv/issues/1740#issuecomment-814001188 )

   1.3. Устанавливаем плагин pyenv-virtualenv https://www.liquidweb.com/kb/how-to-install-pyenv-virtualenv-on-ubuntu-18-04/

   1.4. Локально в папке с проектом прописываем версию python `pyenv local 3.9.13`

   1.5. Перезагружаем терминал

   1.6. Создаём песочницу `pyenv virtualenv 3.9.13  mailing_service_3.9.13`

   1.7. Активируем песочницу `pyenv activate mailing_service_3.9.13`

   1.8 Устанавливаем все необходимые библиотеки `pip install -r requirements.txt`

   1.9 Локально запускаем файл main.py, demon_mailing.py 



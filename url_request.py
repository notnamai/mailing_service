""" Функция подключается к внешнему API."""
import asyncio
import dataclasses
import uuid
from datetime import (
    date,
    datetime,
    )
from typing import (
    Any,
    Optional,
    Union,
    )

import aiohttp
import simplejson
from aiohttp import (
    BasicAuth,
    ClientConnectorError,
    )
from simplejson import JSONEncoder as BaseEncoder


def dict_pre_dump(data: Union[dict, list, tuple, Any]) -> Union[dict, list, str, int, float]:
    """
    Обходим словарь в глубину и переводим типы и функции, которые simplejson не сможет сериализовать в такие типы,
    которые сможет сериализовать.

    :param data: Словарь с исх. данными
    :return: Обработанный словарь
    """
    if isinstance(data, (list, tuple,)):
        new_list = []
        for i_val in data:
            new_list.append(dict_pre_dump(i_val))
        return new_list
    elif isinstance(data, dict):
        for i_key in list(data.keys()):
            i_val = data[i_key]
            data[i_key] = dict_pre_dump(i_val)
        return data
    elif isinstance(data, (uuid.UUID,)):
        return str(data)
    elif isinstance(data, (datetime, date)):
        return data.isoformat()
    elif dataclasses and dataclasses.is_dataclass(data):
        return dataclasses.asdict(data)

    return data


class JSONEncoder(BaseEncoder):
    def default(self, obj):
        return dict_pre_dump(obj)


class ErrorStatus(Exception):
    pass


async def url_request(
    url: str,
    loop: Any = None,
    repeat: int = 10,
    response_status: int = 200,
    type_request: str = 'get',
    data: Optional[dict] = None,
    json: Optional[dict] = None,
    headers: Optional[dict] = None,
    auth: Optional[BasicAuth] = None,
) -> dict:
    type_request: str = type_request if type_request in ['get', 'post', 'put', 'patch'] else 'get'
    _data: Optional[dict] = {}
    _data.update({'data': data} if data and type_request in ['post', 'put', 'patch'] else {})
    _data.update({'json': dict_pre_dump(json)} if json and type_request in ['post', 'put', 'patch'] else {})
    loop = loop or asyncio.get_event_loop()
    for _ in range(repeat):
        try:
            async with aiohttp.ClientSession(loop=loop, headers=headers, auth=auth) as client:
                async with getattr(client, type_request)(url, **_data) as response:
                    if response.status != response_status:
                        raise ErrorStatus(response)
                    response_data = await response.read()
                    data: dict = simplejson.loads(response_data.decode('utf-8'))
                    print(f'Data {data} received at address {url}')
                    return data
        except ClientConnectorError as ex:
            print(ex)
            await asyncio.sleep(1)

FROM python:3.9.13
RUN mkdir /usr/app
WORKDIR /usr/app
COPY ./requirements.txt /usr/app
RUN pip install -r requirements.txt
ENV PYTHONPATH="/usr/app:$PYTHONPATH"
COPY . /usr/app

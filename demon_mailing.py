import asyncio
from asyncio import sleep
from collections import defaultdict
from datetime import (
    datetime,
    timedelta,
    )
from typing import List

from sqlalchemy import select
from sqlalchemy.orm import joinedload

from app.models import (
    Client,
    MailingList,
    Message,
    )
from app.models.database import GetDbManager
from url_request import (
    ErrorStatus,
    url_request,
    )


async def _create_message():
    async with GetDbManager() as session:
        now_utc = datetime.utcnow()
        #действующие рассылки
        mailing_list: List[MailingList] = (await session.execute(select(MailingList).where(MailingList.end_date > now_utc - timedelta(days=1)))).scalars().all()
        clients: List[Client] = (await session.execute(select(Client))).scalars().all()
        message: List[Message] = (await session.execute(select(Message).where(Message.mailing_list_id.in_([m.id for m in mailing_list])))).scalars().all()
        message_search = defaultdict(set)
        for m in message:
            message_search[m.mailing_list_id].add(m.client_id)

        for m in mailing_list:
            for c in clients:
                user_date = now_utc
                if c.time_zone:
                    user_date = now_utc + timedelta(minutes=int(c.time_zone*60))
                if m.date_created.timestamp() < user_date.timestamp() < m.end_date.timestamp() and c.id not in message_search.get(m.id, set()):
                    #создать письмо со статусом sending m.id c.id
                    cur_message = Message(date_create_send=now_utc,
                                          status='sending',
                                          mailing_list_id=m.id,
                                          client_id=c.id)

                    session.add(cur_message)
                    await session.commit()


async def create_message():
    while True:
        try:
            await _create_message()
        except Exception as ex:
            raise
            #print(ex)
        await sleep(1)


async def _send_message():
    async with GetDbManager() as session:
        messages: List[Message] = (await session.execute(
            select(Message).options(joinedload(Message.client), joinedload(Message.mailing_list))
            .where(Message.status == 'sending'))).scalars().all()
        for i_message in messages:
            try:
                res = await url_request(
                    f'https://probe.fbrq.cloud/v1/send/{i_message.id}',
                    headers={
                        'accept': 'application/json',
                        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQ2MTY1NDcsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Indhcm1zbm93MTcifQ.U8PPiyEY3ri-ndxq5hTSwNq9q4-h15OPD6bndgZp1YI',
                        'Content-Type': 'application/json',
                    },
                    type_request='post',
                    json={
                              "id": i_message.id,
                              "phone": int(i_message.client.phone_number),
                              "text": i_message.mailing_list.text_message,
                            }
                )

                #если все прошло хорошо меняем статус сообщения на отправлено
                i_message.status = 'send'
            except ValueError as ex:
                print(f'не удалось отправить сообщение пользователю {i_message.client.id}, на номер {i_message.client.phone_number}', ex)
                i_message.status = 'canceled'
            except ErrorStatus:
                 #если ошибка меняем в базе данных статус сообщения на отменено
                i_message.status = 'canceled'
            print(f'Создалось сообщение {i_message.id}')
            await session.commit()


async def send_message():
    while True:
        try:
            await _send_message()
        except Exception as ex:
            print(ex)
        await sleep(1)


async def main():

    task_1 = asyncio.create_task(create_message())
    task_2 = asyncio.create_task(send_message())

    await task_1
    await task_2


if __name__ == '__main__':
    print('step: asyncio.get_event_loop()')
    asyncio.run(main(), debug=True)







